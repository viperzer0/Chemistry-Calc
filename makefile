VERSION := 0.1.0
CC := cc
CFLAGS := -g
FILENAME := calculator_linux_$(VERSION)
HEADERS = src/main.h

$(FILENAME): src/*.o 
	$(CC) $(inputs) -o $(output) 

obj/%.o: src/%.c $(HEADERS)
	$(CC) $(CFLAGS) -c $(input) -o $(output)
	
