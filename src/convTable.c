/*Name:convTable.c
 *Contains: convTable()
 *Description: Converts an integer table into a fractional table.
 */

#include "main.h"
#include <stdlib.h>
void convTable(int *matPtr, frac *retPtr, int rows, int cols){
	int i;
	for(i=0;i<(rows*cols);i++){
		(retPtr+i)->num=*(matPtr+i);
		(retPtr+i)->den=1;
	}
}	
