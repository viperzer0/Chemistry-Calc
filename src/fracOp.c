#include "main.h"

void reduce(frac *fraction){
	if((fraction->num==0)&&(fraction->den==0)){fraction->num=1;fraction->den=1;return;} //0/0=1. Please don't ask why.
	int divisor=gcd(fraction->num,fraction->den);	
	fraction->num=(fraction->num/divisor);
	fraction->den=(fraction->den/divisor);

}

int massLcd(frac *arr, int len){
	int i=0;
	int factor = lcd((arr+i)->den,(arr+i+1)->den);
	for(i=2;i<len-1;i++){
		factor = lcd(factor,(arr+i+1)->den);
	}
	return factor;
}

frac addFrac(frac op1, frac op2){
	int mult1,mult2,factor=1;
	frac result;
	if((op1.den)!=(op2.den)){
		factor=lcd(op1.den,op2.den);
	}
	mult1=factor/op1.den;
	mult2=factor/op2.den;
	result.num=op1.num*mult1+op2.num*mult2;
	result.den=factor;	
	return result;	
}

frac mulFrac(frac op1, frac op2){
	frac result;
	result.num=op1.num*op2.num;
	result.den=op1.den*op2.den;
	return result;
}

frac inv(frac op1){
	frac result;
	result.num=op1.den;
	result.den=op1.num;
	return result;
}
