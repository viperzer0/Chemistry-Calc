#include "main.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define RED     "\x1b[31m"
#define RESET   "\x1b[0m"

void dispEqu(char *equation, frac *nums){
	int j=0;
	int len=strlen(equation);	
	printf(RED "%d" RESET,abs(nums->num));
	nums++;
	while(j<len){
		if(equation[j+1]=='+'||equation[j+1]=='='){
			printf("%c %c ",equation[j],equation[j+1]);
			printf(RED "%d" RESET,abs(nums->num));
			nums++;
			j+=2;
			continue;	
		}
		printf("%c",equation[j]);
		j++;
	}
}
void displayMatrix(int *matPtr, int rows, int columns){
	int index=0,i,j;
	for(i=0;i<rows;i++){
		for(j=0;j<columns;j++){
			printf("%d\t",*(matPtr+(i*columns)+j));
		}
		printf("\n");
	}
}

void displayFMatrix(frac *matPtr, int rows, int columns){
	int index=0,i,j;
	for(i=0;i<rows;i++){
		for(j=0;j<columns;j++){
			printf("%d/%d\t",((matPtr+(i*columns)+j)->num),((matPtr+(i*columns)+j)->den));
		}
		printf("\n");
	}
	printf("\n");
}

void dispArr(frac *arrPtr, int len){
	int i;
	for(i=0;i<len;i++){
		printf("%d/%d,",(arrPtr+i)->num,(arrPtr+i)->den);
	}
	printf("\n");
}
	
