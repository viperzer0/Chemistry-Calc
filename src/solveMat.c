/*Name: solveMat.c
 *Contains: solveMat()
 *Description: Solves a matrix (system of linear equations, i.e, chemical equations.)
 *Notes: Does not work yet. Needs revision and a new algorithm.
 */

#include "main.h"
#include <stdlib.h>
void solveMat(frac *matPtr, int rows, int cols){
	int i,j,cur;	
	for(i=0;i<cols-1;i++){
		cur=i;	
		if((matPtr+getPos(rows,cols,i,i))->num==0){ //Might not be neccessary?
			do{
				cur++;
			}while(((matPtr+getPos(rows,cols,cur,i))==0)&&(cur<rows));
			swapR(matPtr,cols,i,cur);
		}
		for(j=0;j<rows;j++){
			if((matPtr+getPos(rows,cols,i,i))->num==0)continue;	
			if((matPtr+getPos(rows,cols,j,i))->num==0)continue;
			if(i==j)continue;
			frac elim;
			elim.num=(matPtr+getPos(rows,cols,j,i))->num*(matPtr+getPos(rows,cols,i,i))->den*-1;
			elim.den=(matPtr+getPos(rows,cols,i,i))->num*(matPtr+getPos(rows,cols,j,i))->den;
			addR(matPtr,cols,i,j,elim);
		}
	}
	for(i=0;i<rows;i++){
		if((matPtr+getPos(rows,cols,i,i))->num!=1){
			mulR(matPtr,cols,i,inv(*(matPtr+getPos(rows,cols,i,i))));
		}
	}
	for(i=0;i<rows*cols;i++)
		reduce(matPtr+i);
}

void retMatVals(frac *matPtr, frac *finalPtr, int rows, int cols){
	int i;
	for(i=0;i<rows;i++){
		(finalPtr+i)->num=(matPtr+getPos(rows,cols,i,cols-1))->num;
		(finalPtr+i)->den=(matPtr+getPos(rows,cols,i,cols-1))->den;
	}
}	
