## Synopsis
Chemistry-Calc is a program that allows you to perform several complicated proceedures involving chemicals automatically.
At the moment, Chemistry Calc only supports viewing properties of elements and balancing chemical equations.

## Installation
Download the repository and extract it to wherever you want it, and run `make`. On Linux, mark the Calculator program as executable and run it with `./calculator_linux_[version]`. 
On Windows, Calculator should be able to run in the terminal, though further testing is needed.

## Running
Chemistry-Calc runs only through an interactive, console environment, with no command line arguments. I plan on implementing a CLI soon, however.
After starting Calculator, simply supply the desired arguments as it asks for them. Balancing chemical equations in particular is finicky, read the built in instructions on how to input equations
properly. 

## Contributors
All contributions are done right here in GitLab. Feel free to fork the project and help out where you can! Anything you think can be helpful is no doubt appreciated.

## Importing periodic table.
Chemistry Calc uses a modified version of a .csv file released [here](http://php.scripts.psu.edu/djh300/cmpsc221/p3s11-pt-data.htm) for its periodic data.
It has been modified to exclude the CPK color data (originally fifth column) that I found irrelevant for my purposes. 
If you wish to make or use your own .csv file, make sure it fits the following criteria:
*Columns are separated by a single tab character.
*Lines are separated by a newline character.
*Blank columns are okay. (0's are inserted during the program runtime. This is neccessary for file parsing, but idk why.)

The order is as follows:
1. Atomic Number 
2. Symbol [3 characters]
3. Name [20 characters]
4. Atomic Mass in g/mol
5. Electron Configuration [100 characters]
6. Electronegativity in Pauling 
7. Atomic radius in pm 
8. Ion radius in pm 
9. Van der Waals radius in pm
10. IE-1 in kJ/mol 
11. EA in kJ/mol
12. Oxidation states [40 characters]
13. Standard state [7 characters] (Solid, liquid, or gas)
14. Bonding type [17 characters] 
15. Melting point in K
16. Boiling point in K
17. Density in g/mL
18. Metal or nonmetal [40 characters] (String, takes values such as "metal","metalloid","transition metal","halogen","lanthanoid", etc.)
19. Year discovered [7 characters] (Year number or "Ancient")
